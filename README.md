# passwordgen

# Program name

passgen.py

# Language

Python 3

# Description

A basic password generator based on code for a random quotation generator. 

This program produces, when executed:-

* Three random words
* A Random number 1-9
* A random Character from the number keys. does not include "

Should be fairly easy to follow

# BUGS

None

# TODO

* Add more words, 
* Produce 2 random numbers and characters

# HELP

If you would like some help please get in touch

# Contact

paulsutton@disroot.org
