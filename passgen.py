import random

RESPONSES = ["Lamp",
             "Mouse",
             "Keys",
             "Cup",
             "Book",
             "Battery",
             "Pen",
             "Outlook good",
             "Yes"] 
             
RESPONSES2 = ["Table",
             "Chair",
             "Bed",
             "Sheet",
             "Duvet",
             "Head",
             "Board",
             "Drawer",
             "Curtain"]            
             
RESPONSES3 = ["Plug",
             "Socket",
             "Resistor",
             "Connector",
             "Capacitor",
             "Transistor",
             "Speaker",
             "Fuse",
             "Circuit"]      
RESPONSES4 = ["1",
             "2",
             "3",
             "4",
             "5",
             "6",
             "7",
             "8",
             "9"]      
RESPONSES5 = ["!",
             "£",
             "$",
             "%",
             "^",
             "&",
             "*",
             "(",
             ")"]            
 
answer = random.choice(RESPONSES)
print(f"First Word of Password: {answer}")
answer2 = random.choice(RESPONSES2)
print(f"Second Word of Password: {answer2}")
answer3 = random.choice(RESPONSES3)
print(f"Third Word of Password: {answer3}")
answer4 = random.choice(RESPONSES4)
print(f"Random Number: {answer4}")
answer5 = random.choice(RESPONSES5)
print(f"Character: {answer5}")
